import cherrypy
import algorithms
import server
import json

from hierarchical_clusterer import HierarchicalClusterer
from regex_creator import RegexCreator

class Supporter:
	@cherrypy.expose
	def get_methods(self):
		algos = [algorithms.BoW_TF_IDF()]
		return "{\"algorithms\":[%s]}"%','.join([str(algo) for algo in algos])

	@cherrypy.expose
	def get_db_documents(self):
		collections = server.MONGO_CLIENT.clustering.clustering
		documents = collections.distinct("name")
		print documents
		return json.dumps(documents)

	@cherrypy.expose
	def clear_db(self):
		server.MONGO_CLIENT.clustering.clustering.remove()

	@cherrypy.expose
	def get_node_label(self):
		cl = cherrypy.request.headers['Content-Length']
		json_str = cherrypy.request.body.read(int(cl))
		
		hc = HierarchicalClusterer(json_str)
		result = u"%s"%hc.get_root_label()

		result = json.dumps(result)
		print result
		return result

	@cherrypy.expose
	def get_node_label2(self):
		cl = cherrypy.request.headers['Content-Length']
		json_str = cherrypy.request.body.read(int(cl))

		tree = json.loads(json_str)
		rc = RegexCreator(tree)
		return rc.get_regex()
