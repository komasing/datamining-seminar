from json import dumps

class Algorithm:
	def __init__(self):
		raise NotImplementedError

	def getName(self):
		""" Returns a string. """
		return self.name

	def getDescription(self):
		""" Returns a string. """
		return self.desc

	def getDistances(self):
		""" Returns a list of maps {name->x, value->y}. """
		return self.distances

	def getMethods(self):
		""" Returns a list of maps {name->x, value->y}. """
		return self.methods

	def __str__(self):
		return "{\"name\":\"%s\",\"desc\":\"%s\",\"methods\":%s,\"distances\":%s}"%(self.name,self.desc,dumps(self.methods),dumps(self.distances))

class BoW_TF_IDF(Algorithm):
	def __init__(self):
		self.name = "BoW TF-IDF"
		self.desc = "Uses bag-of-words model with TF-IDF."
		self.distances = [{"name":"Euclidean","value":"euclidean"},
				{"name":"Manhattan","value":"cityblock"},
				{"name":"Squared Euclidean","value":"sqeucliden"},
				{"name":"Cosine","value":"cosine"},
				{"name":"Correlation","value":"correlation"},
				{"name":"Hamming","value":"hamming"},
				{"name":"Jaccard","value":"jaccard"},
				{"name":"Chebyshev","value":"chebyshev"},
				{"name":"Canberra","value":"canberra"},
				{"name":"Bray-Curtis","value":"braycurtis"},
				{"name":"Mahalanobis","value":"mahalanobis"},
				{"name":"Weighted Minkowsky","value":"wminkowski"}]
		self.methods = [{"name":"Single","value":"single"},
				{"name":"Complete","value":"complete"},
				{"name":"UPGMA","value":"average"},
				{"name":"WPGMA","value":"weighted"},
				{"name":"UPGMC","value":"centroid"},
				{"name":"WPGMC","value":"median"},
				{"name":"Ward","value":"ward"}]
