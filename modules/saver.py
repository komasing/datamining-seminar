import cherrypy
import server
import json

class Saver:
	@cherrypy.expose
	def save(self):
		cl = cherrypy.request.headers['Content-Length']
		rawbody = cherrypy.request.body.read(int(cl))
		document = json.loads(rawbody)
		collections = server.MONGO_CLIENT.clustering.clustering
                collections.insert(document)
		#collections.update("{}")
