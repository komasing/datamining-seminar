class Cluster:

	def __init__(self,fst_child,snd_child=None,distance=0,label="",collapsed=False, cluster="Default"):
		self.fst_child = fst_child
		self.snd_child = snd_child
		self.label = label
		self.cluster = cluster
		self.collapsed = collapsed
		self.distance = distance
		self.height = 0

	def calculate_height(self):
		self.height =  1 + max(self.fst_child.calculate_height(), self.snd_child.calculate_height())
		return self.height

	def __str__(self):
		children_repr = [str(self.fst_child)]
		if self.snd_child != None:
			children_repr.append(str(self.snd_child))
		children_repr = "[%s]"%(','.join(children_repr))
		return "{\"label\":\"%s\",\"cluster\":\"%s\",\"collapsed\":%d,\"distance\":%f,\"height\":%d,\"children\":%s}"%(self.label,self.cluster,int(self.collapsed),self.distance,self.height,children_repr)

	def __unicode__(self):
		children_repr = [unicode(self.fst_child)]
                if self.snd_child != None:
                        children_repr.append(unicode(self.snd_child))
                children_repr = "[%s]"%(','.join(children_repr))
                return u"{\"label\":\"%s\",\"cluster\":\"%s\",\"collapsed\":%d,\"distance\":%f,\"height\":%d,\"children\":%s}"%(self.label,self.cluster,int(self.collapsed),self.distance,self.height,children_repr)

class UniCluster(Cluster):
	
	def calculate_height(self):
		self.height = 0
		return self.height

class Leaf:
	
	def __init__(self,label,cluster="Default"):
		self.label = label
		self.height = 0
		self.cluster = cluster

	def calculate_height_height(self):
		return self.height

	def __str__(self):
		return ("{\"label\":\"%s\",\"cluster\":\"%s\"}"%(self.label,self.cluster))

	def __unicode__(self):
		return u"{\"label\":\"%s\",\"cluster\":\"%s\"}"%(self.label,self.cluster)

def steps_to_tree(steps, data):
	"""
	  steps - output of scipy.cluster.hierarchy.linkage() on data
	  data - initial input, one document per line
	"""
	print data

	# set each leaf to 1-element-cluster
	N = len(data)
	cluster_map = {}
	for i in range(N):
		leaf = Leaf(data[i])
		#cluster_map[i] = UniCluster(leaf,label="Cluster %d"%i)
		cluster_map[i] = UniCluster(leaf)

	next_cluster_idx = N

	# generate clusters of clusters
	for i in range(len(steps)):
		step = steps[i]
		#cluster_map[i+N] = Cluster(cluster_map[int(step[0])],cluster_map[int(step[1])],step[2],"Cluster %d"%(i+N))
		cluster_map[i+N] = Cluster(cluster_map[int(step[0])],cluster_map[int(step[1])],step[2])

	cluster_map[i+N].calculate_height()
	return cluster_map[i+N]
