import urllib
import cherrypy
import server
import json
from hierarchical_clusterer import HierarchicalClusterer


class Loader:
	@cherrypy.expose
	def load_from_url(self,url,method,distance,algo="BoW TF-IDF"):
		data = [line.rstrip() for line in urllib.urlopen(url).readlines()]
		if algo == "BoW TF-IDF":
			response_txt = str(HierarchicalClusterer(data).cluster_from_plain(method=str(method),metric=str(distance)))
		return "{\"name\":\"\",\"method\":\"%s\",\"distance\":\"%s\",\"algo\":\"%s\",\"tree\":%s}"%(method,distance,algo,response_txt)

	@cherrypy.expose
	def load_from_file(self,file_arg,method,distance,algo="BoW TF-IDF"):
		data = [line.rstrip() for line in file_arg.file.readlines()]
		if algo == "BoW TF-IDF":
			response_txt = str(HierarchicalClusterer(data).cluster_from_plain(method=str(method),metric=str(distance))).decode('utf-8')
			print response_txt
		return "{\"name\":\"\",\"method\":\"%s\",\"distance\":\"%s\",\"algo\":\"%s\",\"tree\":%s}"%(method,distance,algo,response_txt)

	@cherrypy.expose
	def load_from_database(self,document_name):
		collections = server.MONGO_CLIENT.clustering.clustering
		print document_name
		result = json.dumps(collections.find_one({"name":document_name},{"tree":1,"name":1,"_id":0}))
		print result
		return result

	@cherrypy.expose
        def load_from_dendrogram(self,method,distance,algo="BoW TF-IDF"):
                cl = cherrypy.request.headers['Content-Length']
                rawbody = cherrypy.request.body.read(int(cl))
		print rawbody
                data = json.loads(rawbody)
		print data
		if algo == "BoW TF-IDF":
                        response_txt = unicode(HierarchicalClusterer(data).cluster_from_plain(method=str(method),metric=str(distance)))
			print response_txt
                return "{\"name\":\"\",\"method\":\"%s\",\"distance\":\"%s\",\"algo\":\"%s\",\"tree\":%s}"%(method,distance,algo,response_txt)
