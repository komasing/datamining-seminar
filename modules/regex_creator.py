import string

class RegexCreator:

	def __init__(self,tree):
		self.tree = tree
		self.trans_table = dict((ord(char),None) for char in string.punctuation)

	def get_regex(self):
		return self.alignment_to_regex_string(self.parse_node(self.tree))

	def parse_node(self,node):
		children = node["children"]
		if len(children) == 1:
			return self.tokenize_label(children[0]["label"])
		else:
			return self.get_pairwise_alignment(self.parse_node(children[0]), self.parse_node(children[1]))
			

	def get_pairwise_alignment(self,seq1,seq2):
		return needleman_wunsch(seq1,seq2)
		
	def tokenize_label(self,label):
		return label.lower().translate(self.trans_table).split()

	def alignment_to_regex_string(self,alignment):
		if len(alignment) == 1:
			return "*"
		table = {ord('-'):ord('*')}
		return ' '.join(alignment).translate(table)

def needleman_wunsch(seq1, seq2, match_score=2, mismatch_penalty=-2, gap_penalty=-2):
	seq1.insert(0,'-')
	seq2.insert(0,'-')

	n = len(seq1)
	m = len(seq2)

	F = [[0]*n for i in range(m)]

	for i in range(m):
		F[i][0] = i*mismatch_penalty
	for j in range(n):
		F[0][j] = j*mismatch_penalty
	
	for j in range(1,n):
		for i in range(1,m):
			match = F[i-1][j-1] +  (match_score if seq1[j]==seq2[i] else mismatch_penalty)
			delete = F[i-1][j] + gap_penalty
			insert = F[i][j-1] + gap_penalty
			F[i][j] = max(match, delete, insert)

	seq1_alignment = []
	seq2_alignment = []

	i = m-1
	j = n-1

	while i > 0 and j > 0:
		if i > 0 and j > 0 and F[i][j] == F[i-1][j-1] + (match_score if seq1[j]==seq2[i] else mismatch_penalty):
			seq1_alignment.insert(0,seq1[j])
			seq2_alignment.insert(0,seq2[i])
			i -= 1
			j -= 1
		elif i > 0 and F[i][j] == F[i-1][j] + gap_penalty:
                        seq1_alignment.insert(0,'-')
                        seq2_alignment.insert(0,seq2[i])
                        i -= 1
		elif j > 0 and F[i][j] == F[i][j-1] + gap_penalty:
			seq1_alignment.insert(0,seq1[j])
			seq2_alignment.insert(0,'-')
			j -= 1

	return aggregate_alignments(seq1_alignment,seq2_alignment,seq1[1:],seq2[1:])

def aggregate_alignments(alignment1, alignment2, source1, source2):
	aggregate = [alignment1[i] if alignment1[i]==alignment2[i] else '-' for i in range(len(alignment1))]

	if aggregate[0] != source1[0] or aggregate[0] != source2[0]:
		aggregate.insert(0,'-')
	if aggregate[-1] != source1[-1] or aggregate[-1] != source2[-1]:
		aggregate.append('-')

	dash_met = False
	i = 0
	while i < len(aggregate):
		if aggregate[i] == '-':
			if dash_met:
				aggregate.pop(i)
			else:
				dash_met = True
				i += 1
		else:
			dash_met = False
			i += 1

	return aggregate
