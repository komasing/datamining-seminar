from sklearn.feature_extraction.text import TfidfVectorizer
import scipy.spatial.distance as dist
import scipy.cluster.hierarchy as hier
from dendrogram import steps_to_tree
import json

class HierarchicalClusterer:
	def __init__(self,data):
		self.data = data

	def cluster_from_plain(self,method="single",metric="cosine"):
		vectorizer = TfidfVectorizer()
		normalised_bow = vectorizer.fit_transform(self.data)
		
		distance_matrix = dist.pdist(normalised_bow.toarray(),metric=metric)
		linkage_matrix = hier.linkage(distance_matrix,method=method)
		print linkage_matrix

		return steps_to_tree(linkage_matrix,self.data)

	def get_root_label(self):
		vectorizer = TfidfVectorizer()
		normalised_bow = vectorizer.fit_transform(parse_segments_from_json(self.data))
		words = vectorizer.get_feature_names()

		norm_bow_arr = normalised_bow.toarray()
		bow_arr_bool = [[True if norm_bow_arr[i,j] else False for j in range(len(norm_bow_arr[0]))] for i in range(len(norm_bow_arr))]

		common_words_bool = reduce(list_and,bow_arr_bool)
		return [words[i] for i in range(len(words)) if common_words_bool[i]]


def list_and(list1,list2):
	n = len(list1)
	return [list1[i] & list2[i] for i in range(n)]

def parse_segments_from_json(data):
	tree = json.loads(data)
	stack = [tree]
	segments = []

	while len(stack) > 0:
		node = stack.pop()
		children = node["children"]
		if len(children) == 1:
			segments.append(children[0]["label"])
		else:
			stack.append(children[0])
			stack.append(children[1])
	
	return segments
