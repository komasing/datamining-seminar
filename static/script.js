//var svg_elem = d3.select("#dendrogramHolder").append("svg").style("position","absolute").style("top",d3.select("#table_header").node().getBoundingClientRect().bottom+"px");

var LINE_WIDTH = 2;
var LINE_COLOUR = "green";
var NODE_DIST = 50;
var NODE_R = 10;

var NODE_COLOUR = "red";
var COLLAPSED_NODE_COLOUR = "blue";
var LEAF_COLOUR = "black";
var SELECTED_NODE_COLOUR = "orange";

var TEXT_COLOUR = "black";

var CLUSTER_BG1 = "#dedede";
var CLUSTER_BG2 = "#bebebe";

var NODE_PADDING = 20;

var EMBEDDED_LINE_LEN = NODE_DIST-2*NODE_R;

var CLUSTER_FORMATION_PRECISION = 100;

var IS_CLUSTERING = false;

var TRIMMED_TEXT = 30;

var DOCUMENT;
var TREE;
var OPTIONS;

var SELECTED_NODE = null;

var HEIGHT_MAP;

ajaxGetOptions();
refreshDBEntries();
thresholdSliderHandlerOnChange(document.getElementById("threshold_slider"));

// init svg
var outer = d3.select("#dendrogramHolder").append("svg")
							.attr("pointer-events","all")
							.on("dblclick.zoom", null);

var vis = outer.append("g").attr("id","g");
//vis.append("rect").attr("width",$(document).width()*100).attr("height",10000).attr("fill","yellow");

/*
vis.call(d3.behavior.zoom().on("zoom",rescale))
							.on("mousedown",mousedown);
							//.on("mousemove",mousemove)
							//.on("mouseup",mouseup);

//d3.event.preventDefault();

function mousedown() {
	//if (!node_selected)
	vis.call(d3.behavior.zoom().on("zoom", rescale));
	drawDendrogram();
	return;
}


// rescale g
function rescale() {
	trans=d3.event.translate;
	scale=d3.event.scale;

	vis.attr("transform",
      "translate(" + trans + ")"
      + " scale(" + scale + ")");
    drawDendrogram();
}
*/

$("#cluster_checkbox").click(function() {
	if (this.checked) {
		IS_CLUSTERING = true;
	} else {
		IS_CLUSTERING = false;
	}
	drawDendrogram();
});

$("body").keydown(keypressHandler);

function JSONFilter(key,value) {
	if (key=="circle"||key=="parent") {
		return undefined;
	} else {
		return value;
	}
}
	
function extractLeafLabels(tree) {
	var labels = new Array();
	
	var visit = function(node) {
		if (node.children == null) {
			labels.push(node.label);
		} else {
			var arrLen = node.children.length;
			for (var i = 0; i < arrLen; i++) {
				visit(node.children[i]);
			}
		}
	};
	visit(tree);
	
	return labels;
}

function getSettings() {
	var elem = document.getElementById("method_selector");
	var method_choice = elem.options[elem.selectedIndex].value;
	
	elem = document.getElementById("distance_selector");
	var distance_choice = elem.options[elem.selectedIndex].value;
	
	return {"method":method_choice, "distance":distance_choice};
}

function ajaxPostFileCallback() {
	initDendrogram(this.responseText);
}

function ajaxPostFile(formElement) {
	var req = new XMLHttpRequest();
	req.onload = ajaxPostFileCallback;
	var query = "loader/load_from_file?";
	
	var settings = getSettings();
	
	query = query + "method="+settings.method + "&distance="+settings.distance;
			
	req.open("post",query,true);
	req.send(new FormData(formElement));
}

function ajaxGetFiles() {
	var req = new XMLHttpRequest();
	req.onreadystatechange=function() {
		if (req.readyState==4 && req.status==200) {
  			initDendrogram(req.responseText);	
    	}
	};
	
	var query = "loader/load_from_url?";
	var settings = getSettings();
	
	var elem = document.getElementById("file_url");
	var choice = elem.value;
	
	query = query + "url=" + choice + "&method="+settings.method + "&distance="+settings.distance;
	
	req.open("get",query,true);
	req.send();
	
}

function applySettings() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status==200) {
  			initDendrogram(xhr.responseText);	
    	}
	};
	
	var labels = extractLeafLabels(TREE);
	var settings = getSettings();
	
	var query = "loader/load_from_dendrogram?method=" + settings.method + "&distance=" + settings.distance;
	
	var data = JSON.stringify(labels);
	
	xhr.open("post",query,true);
	xhr.send(data);	
}

function ajaxGetOptions() {
	var req = new XMLHttpRequest();
	req.onreadystatechange=function() {
  		if (req.readyState==4 && req.status==200) {
    		OPTIONS = JSON.parse(req.responseText);
    		fillAlgoSelection();
    	}
  };
	
	req.open("get","supporter/get_methods");
	req.send();
}

function save() {
	var xhr = new XMLHttpRequest();
	if (DOCUMENT.name === "") {
		DOCUMENT.name = prompt("Please enter the name of the document",DOCUMENT.name);	
	}
	var doc = JSON.stringify(DOCUMENT,JSONFilter);
	xhr.open("post","saver/save",true);
	xhr.send(doc);
	
	refreshDBEntries();
}

function saveAs() {
	var xhr = new XMLHttpRequest();
	DOCUMENT.name = prompt("Please enter the name of the document",DOCUMENT.name);
	var doc = JSON.stringify(DOCUMENT,JSONFilter);
	xhr.open("post","saver/save",true);
	xhr.send(doc);
	
	refreshDBEntries();
}

function getNodeLabel(node) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status==200) {
			node.label = xhr.responseText.replace(/\"/g, "");
			drawDendrogram();
		}
	};
	xhr.open("post","supporter/get_node_label2",true);
	xhr.send(JSON.stringify(node,JSONFilter));
}

function refreshDBEntries() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status==200) {
			renewDBEntrySelector(JSON.parse(xhr.responseText));
		}
	};
	xhr.open("get","supporter/get_db_documents", true);
	xhr.send();
}

function renewDBEntrySelector(options) {
	var selector = document.getElementById("db_entry_selector");
	
	while (selector.hasChildNodes()) {
		selector.removeChild(selector.lastChild);
	}
	
	for (var i = 0; i < options.length; i++) {
		selector[i] = new Option(options[i],options[i],false,false);
	}
} 

function empty_db() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status==200) {
			refreshDBEntries();
		}
	};
	xhr.open("delete","supporter/clear_db",true);
	xhr.send();
}

function loadFromDB() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status==200) {
			initDendrogram(xhr.responseText);
		}
	};
	
	var selector = document.getElementById("db_entry_selector");
	var choice = selector.options[selector.selectedIndex].value;
	
	xhr.open("get","loader/load_from_database?document_name="+choice, true);
	xhr.send();
}

function initSlider() {
	var min_max = findMinMaxDistance(TREE);
	$("#min_threshold").text(min_max.min);
	$("#max_threshold").text(min_max.max);
	$("#threshold_slider").attr("min",min_max.min).attr("max",min_max.max);
}

function initDendrogram(json) {
	DOCUMENT = JSON.parse(json);
	TREE = DOCUMENT.tree;
		
	initSlider();	

	drawDendrogram(TREE);	
}

function fillAlgoSelection() {
	var algo_selector = document.getElementById('algorithm_selector');
	algo_selector.onchange = algorithmSelectorHandler;
	algo_selector[0] = new Option("","default",true,false);
	
	var algorithms = OPTIONS.algorithms;
	for (var i=0; i < algorithms.length;i++) {
		var algo = algorithms[i];
		algo_selector[i+1] = new Option(algo.name,i,false,false);		
	}
}

function algorithmSelectorHandler(selectee) {
	var algo = OPTIONS.algorithms[selectee.target.value];
	$("#algo_desc").text("");
	
	var method_selector = document.getElementById('method_selector');
	var distance_selector = document.getElementById('distance_selector');
		
	while(method_selector.lastChild) {
		method_selector.removeChild(method_selector.lastChild);
	}
	while(distance_selector.lastChild) {
		distance_selector.removeChild(distance_selector.lastChild);
	}
	
	if (selectee.target.value !== "default") {
		$("#algo_desc").text(algo.desc);
		for (var i = 0; i < algo.methods.length; i++) {
			var method = algo.methods[i];
			method_selector[i] = new Option(method.name,method.value,false,false);
		}
			
		for (var i = 0; i < algo.distances.length; i++) {
			var distance = algo.distances[i];
			distance_selector[i] = new Option(distance.name,distance.value,false,false);
		}
	}
}

function drawHorizontalLine(group,x1,x2,y) {
	append("line",group)
					.attr("x1",x1)
					.attr("x2",x2)
					.attr("y1",y)
					.attr("y2",y)
					.attr("stroke-width",LINE_WIDTH)
					.attr("stroke",LINE_COLOUR);
}

function drawVerticalLine(group,x,y1,y2) {
	append("line",group)
					.attr("x1",x)
					.attr("x2",x)
					.attr("y1",y1)
					.attr("y2",y2)
					.attr("stroke-width",LINE_WIDTH)
					.attr("stroke",LINE_COLOUR);
}

function drawHorLineAndNodeFixed(group, height_padding, node_colour) {
	return drawHorLineAndNode(group, NODE_R, height_padding, node_colour);
}

function drawHorLineAndNode(group, y, height_padding, node_colour) {
	append("line",group).attr("x1",0)
								.attr("x2",EMBEDDED_LINE_LEN+NODE_DIST*height_padding)
								.attr("y1",y)
								.attr("y2",y)
								.attr("stroke-width",LINE_WIDTH)
								.attr("stroke",LINE_COLOUR);
	var node = append("circle",group).attr("cx",NODE_DIST-NODE_R+NODE_DIST*height_padding)
												.attr("cy",y)
												.attr("r",NODE_R)
												.attr("fill", node_colour);
	return node;
}


function SVG(tag)
{
   return document.createElementNS('http://www.w3.org/2000/svg', tag);
}

function append(elem,parent) {
	return $(SVG(elem)).appendTo(parent);
}

function drawDendrogram() {
	
	var drawNode = function(node,group,height_padding) {
		var $g = append("g",group);
		
		var connecting_line_y;

		if (node.children.length == 1 || node.collapsed == 1) {
			var nodeCircle = drawHorLineAndNodeFixed($g,height_padding, node.children.length == 1 ? LEAF_COLOUR : node.selected == 1 ? SELECTED_NODE_COLOUR : COLLAPSED_NODE_COLOUR).data("data",node).click(selectHandler);
			append("text",$g).attr("x",(height_padding+1)*NODE_DIST+NODE_R+5)
									.attr("y",NODE_R+5)
									.attr("fill",TEXT_COLOUR)
									.text("" + (node.children.length == 1? trimText(node.children[0].label) : trimText(node.label)))
									.data("node",node)
									.click(function() {
										alert(node.children.length == 1? $(this).data("node").children[0].label : $(this).data("node").label);
									});
		
			node.circle = nodeCircle;
			
			if (node.selected == 1) {
				SELECTED_NODE = nodeCircle;
			}
			
			
			connecting_line_y = NODE_R;
		} else {
			
			var child1, child2;
			var child1_padding, child2_padding;

			if (node.children[0].collapsed == 1 && node.children[0].height > 0) {
				child1_padding = node.children[0].height;
			} else {
				child1_padding = node.height-node.children[0].height-1;
			}

			if (node.children[1].collapsed == 1 && node.children[1].height > 0) {
				child2_padding = node.children[1].height;
			} else {
				child2_padding = node.height-node.children[1].height-1;
			}
			
			if (child1_padding == child2_padding) {
				child1_padding = 0;
				child2_padding = 0;
			}

			child1 = drawNode(node.children[0],$g,child1_padding);
			child2 = drawNode(node.children[1],$g,child2_padding);
			
			var sub_g1 = child1.g;
			var sub_g2 = child2.g;
			var sub_g1_dim = sub_g1[0].getBoundingClientRect();
			var sub_g2_dim = sub_g2[0].getBoundingClientRect();
			
			var padding = NODE_DIST*height_padding;
			
			sub_g1.attr("transform","translate(" + (NODE_DIST+padding) + ",0)");
			sub_g2.attr("transform","translate("+ (NODE_DIST+padding) + ","+(sub_g1_dim.height+NODE_PADDING)+")");
			
			var child1_liney = child1.line_y;
			var child2_liney = $g[0].getBoundingClientRect().height - sub_g2[0].getBoundingClientRect().height + child2.line_y;
			
			drawVerticalLine($g,NODE_DIST-NODE_R+padding,child1_liney,child2_liney);
			drawHorizontalLine($g,NODE_DIST-NODE_R+padding,NODE_DIST+padding,child1_liney);
			drawHorizontalLine($g,NODE_DIST-NODE_R+padding,NODE_DIST+padding,child2_liney);
			
			connecting_line_y = (child1_liney+child2_liney)/2;
			
			var nodeCircle = drawHorLineAndNode($g, connecting_line_y, height_padding, node.selected == 1 ? SELECTED_NODE_COLOUR : NODE_COLOUR).data("data",node).click(selectHandler);
			
			node.circle = nodeCircle;
			
			if (node.selected == 1) {
				SELECTED_NODE = nodeCircle;
			}
			
			append("text",$g).attr("x",NODE_DIST+5+padding)
									.attr("y",connecting_line_y+5)
									.attr("fill",TEXT_COLOUR)
									.text("" + Math.round(node.distance*100)/100);
		}
		return {"line_y":connecting_line_y,"g":$g};
	};
	
	calculateHeights();
	
	$("#g").empty();
	
	var g = $("#g");
	drawNode(TREE,g,0);
	var dim = g[0].getBoundingClientRect();
	$("svg").css("height",dim.height).css("width",dim.width);
	
	if (IS_CLUSTERING) {
		createClusters();
	}
	
}

function selectHandler() {
	var current_node = $(this);
	if (current_node.data("data").children.length == 1) {
		return;
	}
	if (SELECTED_NODE == null || current_node[0] !== SELECTED_NODE[0]) {
		if (SELECTED_NODE != null) {
			var node_data = SELECTED_NODE.data("data");
			SELECTED_NODE.attr("fill",node_data.collapsed == 1 ? COLLAPSED_NODE_COLOUR : NODE_COLOUR);
			node_data.selected = 0;
		}
		SELECTED_NODE = current_node;
		SELECTED_NODE.attr("fill",SELECTED_NODE_COLOUR);
		SELECTED_NODE.data("data").selected = 1;
	} else {
		var node_data = SELECTED_NODE.data("data");
		SELECTED_NODE.attr("fill",node_data.collapsed == 1 ? COLLAPSED_NODE_COLOUR : NODE_COLOUR);
		node_data.selected = 0;
		SELECTED_NODE = null;
	}
}

function collapseHandler() {
	var node = $(this).data("data");
	node.collapsed = !node.collapsed;
	if (node.collapsed && node.label === "") {
		getNodeLabel(node);
	} else {
		drawDendrogram();
	}
}

function keypressHandler(event) {
	var code = event.keyCode || event.which;
	if (code == 13) {
		if (SELECTED_NODE == null) return;
		
		var node = SELECTED_NODE.data("data");
		node.collapsed = !node.collapsed;
		if (node.collapsed && node.label === "") {
			getNodeLabel(node);
		} else {
			drawDendrogram();
		}
	} else if (code == 9) {
		event.preventDefault();
		if (SELECTED_NODE == null) return;
		
		var node = SELECTED_NODE.data("data");
		var tmp = node.children[0];
		node.children[0] = node.children[1];
		node.children[1] = tmp;
		drawDendrogram();
	} else if (code == 38) {
		if (event.shiftKey) {
			var node_data = SELECTED_NODE.data("data");
			var height_arr = HEIGHT_MAP[node_data.height];
			
			var current_idx = height_arr.indexOf(node_data);
			var next_idx = current_idx-1;
			
			if (current_idx == 0) {
				next_idx = height_arr.length-1;
			}
			
			node_data.selected = 0;
			SELECTED_NODE.attr("fill", node_data.collapsed == 1 ? COLLAPSED_NODE_COLOUR : NODE_COLOUR);
			SELECTED_NODE = height_arr[next_idx].circle;
			
			node_data = SELECTED_NODE.data("data");
			node_data.selected = 1;
			SELECTED_NODE.attr("fill", SELECTED_NODE_COLOUR);			
		} else {
			var node_data = SELECTED_NODE.data("data");
			if (node_data.children[0].children.length == 1 || node_data.collapsed == 1) {
				return;
			}
			node_data.selected = 0;
			SELECTED_NODE.attr("fill", node_data.collapsed == 1 ? COLLAPSED_NODE_COLOUR : NODE_COLOUR);
			SELECTED_NODE = node_data.children[0].circle;
			
			node_data = SELECTED_NODE.data("data");
			node_data.selected = 1;
			SELECTED_NODE.attr("fill", SELECTED_NODE_COLOUR);
		}
	} else if (code == 40) {
		if (event.shiftKey) {
			var node_data = SELECTED_NODE.data("data");
			var height_arr = HEIGHT_MAP[node_data.height];
			
			var current_idx = height_arr.indexOf(node_data);
			var next_idx = current_idx+1;
			
			if (current_idx == height_arr.length-1) {
				next_idx = 0;
			}
			
			node_data.selected = 0;
			SELECTED_NODE.attr("fill", node_data.collapsed == 1 ? COLLAPSED_NODE_COLOUR : NODE_COLOUR);
			SELECTED_NODE = height_arr[next_idx].circle;
			
			node_data = SELECTED_NODE.data("data");
			node_data.selected = 1;
			SELECTED_NODE.attr("fill", SELECTED_NODE_COLOUR);
		} else {
			var node_data = SELECTED_NODE.data("data");
			if (node_data.children[1].children.length == 1 || node_data.collapsed == 1) {
				return;
			}
			node_data.selected = 0;
			SELECTED_NODE.attr("fill", node_data.collapsed == 1 ? COLLAPSED_NODE_COLOUR : NODE_COLOUR);
			SELECTED_NODE = node_data.children[1].circle;
			
			node_data = SELECTED_NODE.data("data");
			node_data.selected = 1;
			SELECTED_NODE.attr("fill", SELECTED_NODE_COLOUR);
		}
	} else if (code == 37) {
		var node_data = SELECTED_NODE.data("data");
		if (node_data.parent == null) {
			return;
		}
		node_data.selected = 0;
		SELECTED_NODE.attr("fill", node_data.collapsed == 1 ? COLLAPSED_NODE_COLOUR : NODE_COLOUR);
		SELECTED_NODE = node_data.parent.circle;
		
		node_data = SELECTED_NODE.data("data");
		node_data.selected = 1;
		SELECTED_NODE.attr("fill", SELECTED_NODE_COLOUR);
	}
}

/*
function selectHandler(target) {
	alert("" + target.value + $("#selector").val());
}
*/
function thresholdSliderHandlerOnChange(target) {
	var valueHolder = document.getElementById("threshold_value");
	valueHolder.value = target.value;
}

function thresholdSliderHandlerOnMouseup() {
	drawDendrogram();
}

function thresholdValueHandler(target) {
	document.getElementById("threshold_slider").value = target.value;
}

function findMinMaxDistance(tree) {
	var minn = Infinity;
	var maxx = -Infinity;
	
	var decide = function(node) {
		console.log(node);
		if (node.children.length == 1) {
			return;
		}
		
		minn = Math.min(minn,node.distance);
		maxx = Math.max(maxx,node.distance);
		
		decide(node.children[0]);
		decide(node.children[1]);
		
	};
	
	decide(tree);
	
	var slider = document.getElementById("threshold_slider");
	var new_minn = Math.round((minn-1/CLUSTER_FORMATION_PRECISION)*CLUSTER_FORMATION_PRECISION)/CLUSTER_FORMATION_PRECISION;
	var new_maxx = Math.round((maxx+1/CLUSTER_FORMATION_PRECISION)*CLUSTER_FORMATION_PRECISION)/CLUSTER_FORMATION_PRECISION;
	slider.value = (new_minn+new_maxx)/2;
	
	thresholdSliderHandlerOnChange(slider);
	
	return {"min":new_minn,"max":new_maxx};
}

function trimText(text) {
	if (text.length > TRIMMED_TEXT) {
		return text.substring(0,TRIMMED_TEXT-1);
	} else if (text.length < TRIMMED_TEXT) {
		return text + Array(TRIMMED_TEXT - text.length + 1).join(" ");
	} else {
		return text;
	}
}

function prependSVG(elem,parent) {
	$e = $(SVG(elem));
	parent.prepend($e);
	return $e;
}

function createClusters() {
	var threshold = $("#threshold_slider").val();
	var draw_bg1 = 1;
	
	var createCluster = function(group) {
		var node = group.children("circle").data("data");

		if (node.distance <= threshold || node.children.length == 1) {
			var dimm = group[0].getBoundingClientRect();

			append("text",group).attr("x",dimm.width + 10).attr("y",dimm.height/2+5).attr("width",TRIMMED_TEXT)
				.text(node.cluster).data("node",node)
						.click(function() {
							var new_name = prompt("New cluster name:",$(this).text());
							$(this).text(new_name);
							$(this).data("node").cluster = new_name;
						});

			dimm = group[0].getBoundingClientRect();
			prependSVG("rect",group).attr("x",0)
									.attr("y",0)
									.attr("width",dimm.width)
									.attr("height",dimm.height)
									.attr("fill",draw_bg1 ? CLUSTER_BG1 : CLUSTER_BG2);
			draw_bg1 = !draw_bg1;
		} else if (!node.collapsed) {
			var sub_groups = group.children("g");
			createCluster(sub_groups.first());
			createCluster(sub_groups.last());
		}
		
	};
	
	createCluster($("svg > g > g"));
	
	var g = $("#g");
	var dim = g[0].getBoundingClientRect();
	$("svg").css("height",dim.height).css("width",dim.width);
	
}

function calculateHeights() {
	
	var height_map = new Array();
	
	var calculateHeight = function(node,parent) {
		
		node.parent = parent;
		
		if (node.collapsed || node.children.length === 1) {
			node.height = 0;
			if (node.collapsed) {
				if (height_map[0] == undefined) {
					height_map[0] = new Array();
				}
				height_map[0].push(node);
			}
			return node.height;
		}
		
		node.height = Math.max(calculateHeight(node.children[0],node),calculateHeight(node.children[1],node)) + 1;
		if (height_map[node.height] == undefined) {
			height_map[node.height] = new Array();
		}
		height_map[node.height].push(node);
		
		return node.height;
		
	};
	calculateHeight(TREE,null);
	HEIGHT_MAP = height_map;
}

function save_svg() {
	alert($("svg")[0].innerHTML);
}
